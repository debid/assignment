import java.util.Scanner;
class Multiplication{
 public static void main(String[] args) {
        System.out.print("Enter a integer number to print multiplication table: ");
        Scanner Scanner = new Scanner(System.in);
        int number = Scanner.nextInt();
        for (int i=1; i<=10; i++){
            System.out.printf("%d * %d = %d\n", number, i, number*i);
        }
    }
}